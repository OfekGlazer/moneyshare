
import React, { useState, useEffect, createContext } from 'react';
import { Text, TouchableOpacity, StyleSheet, View, SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from "./screens/Home/Home";
import Login from "./screens/Login/Login";
import AddGroup from "./screens/Home/AddGroup";
import AddMember from "./screens/Home/AddMember";
import { User } from './Models/General';
import { checkUserSession } from './screens/Service';
import AllGroups from "./screens/Home/AllGroups";
import Regsiter from "./screens/Register/Regsiter";
import MainNavigation from "./Routing/MainNavigation";
import { userContext } from "./AppContext";

const Stack = createNativeStackNavigator();

// export const userContext = createContext<User | undefined | null>(undefined)

const App = () => {

  const [user, setUser] = useState<User | undefined | null>(undefined);

  useEffect(() => {
    checkUserSession(setUser)
  }, [])

  return (
    <userContext.Provider value={{ user, setUser }}>
      <SafeAreaView style={styles.container}>
        <MainNavigation />
      </SafeAreaView>
    </userContext.Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    // alignItems: '',
    justifyContent: 'center',
  },
  startGroupButton: {
    fontSize: 18,
    marginRight: 10,
    color: "#2970E9"
  }
})

export default App;



// return (
//   <NavigationContainer>
//     <userContext.Provider value={user}>

//       <Stack.Navigator>
//         <Stack.Screen name="Home" component={Home} />
//         <Stack.Screen name="Login" component={Login} options={{ title: 'Login' }} />
//         <Stack.Screen name="Regsiter" component={Regsiter} options={{ title: 'Regsiter' }} />
//         <Stack.Screen name="AddGroup" component={AddGroup} />
//         <Stack.Screen name="AllGroups" component={AllGroups} />
//         <Stack.Screen name="AddMember" component={AddMember} options={
//           ({ navigation }) => ({
//             title: 'Add Member',

//             headerRight: () => (

//               <TouchableOpacity
//                 onPress={() => navigation.navigate('AddGroup')}>
//                 <Text style={styles.startGroupButton}>Done</Text>
//               </TouchableOpacity>
//             ),
//           })
//         } />
//       </Stack.Navigator>
//     </userContext.Provider>
//   </NavigationContainer>
// );