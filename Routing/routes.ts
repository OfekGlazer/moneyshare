// File: src/routing/routes.ts
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Expanse, Group } from '../Models/General';

export enum MainRoutes {
    Home = 'Home',
    Login = 'Login',
    Regsiter = 'Regsiter',
    AddGroup = 'AddGroup',
    AllGroups = 'AllGroups',
    AddMember = 'AddMember',
    MenuGroup = 'MenuGroup',
    AddExpense = 'AddExpense',
    ChoosePayingMember = 'ChoosePayingMember',
}

export type MainStackParamList = {
    [MainRoutes.Home]: undefined
    [MainRoutes.Login]: undefined
    [MainRoutes.Regsiter]: undefined
    [MainRoutes.AddGroup]: undefined
    [MainRoutes.AllGroups]: undefined
    [MainRoutes.AddMember]: { setGroupMembers: React.Dispatch<React.SetStateAction<string[]>>, groupMembers: string[] }
    [MainRoutes.MenuGroup]: { group: Group }
    [MainRoutes.AddExpense]: { group: Group }
    [MainRoutes.ChoosePayingMember]: { members: string[], expanse: Expanse, setExpanse: React.Dispatch<React.SetStateAction<Expanse>> }
    // [MainRoutes.Home]: { update: boolean } | undefined // just an example, "update" will later be used for version checks
    // [MainRoutes.Home]: { update: boolean } | undefined // just an example, "update" will later be used for version checks
}

export const MainStack = createNativeStackNavigator<MainStackParamList>()

