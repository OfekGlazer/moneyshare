import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { MainStack, MainRoutes } from './routes'

import Home from '../screens/Home/Home'
import Login from '../screens/Login/Login'
import AddGroup from '../screens/Home/AddGroup'
import Register from '../screens/Register/Regsiter'
import AllGroups from '../screens/Home/AllGroups'
import AddMember from '../screens/Home/AddMember'
import MenuGroup from '../screens/GroupActions/MenuGroup'
import AddExpense from '../screens/GroupActions/AddExpense'
import ChoousePayingMember from '../screens/GroupActions/ChoousePayingMember'

const MainNavigation = (): React.ReactElement => {

    return (
        <NavigationContainer>
            <MainStack.Navigator>
                <MainStack.Screen name={MainRoutes.Home} component={Home} />
                <MainStack.Screen name={MainRoutes.Login} component={Login} />
                <MainStack.Screen name={MainRoutes.Regsiter} component={Register} />
                <MainStack.Screen name={MainRoutes.AddGroup} component={AddGroup} />
                <MainStack.Screen name={MainRoutes.AllGroups} component={AllGroups} />
                <MainStack.Screen name={MainRoutes.AddMember} component={AddMember} />
                <MainStack.Screen name={MainRoutes.MenuGroup} component={MenuGroup} />
                <MainStack.Screen name={MainRoutes.AddExpense} component={AddExpense} />
                <MainStack.Screen name={MainRoutes.ChoosePayingMember} component={ChoousePayingMember} />
            </MainStack.Navigator>
        </NavigationContainer>
    )
}
export default MainNavigation
