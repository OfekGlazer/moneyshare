import React from "react";
import { User } from "./Models/General";

export interface userContext {
    user: User | undefined | null,
    setUser: React.Dispatch<React.SetStateAction<User | null | undefined>>
}

export const userContext = React.createContext<userContext>({ user: null, setUser: () => null })
