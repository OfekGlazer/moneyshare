import { TextProps } from "react-native";
import { IconProps } from "react-native-vector-icons/Icon";



export interface User {
    id: string,
    fullName: string,
    email: string
}

export interface PartialPayment {
    member: string,
    amount: number
}

export interface Expanse {
    title: string,
    amount: string,
    date: Date,
    paidBy: string,
    partialPayments: PartialPayment[]
}

export interface Category {
    name: string,
    icon: any,
    isSelected: boolean,
}

export interface Group {
    id: string,
    userID: string,
    title: string,
    description: string,
    category: Category
    members: string[],
    expanses: Expanse[]
}
