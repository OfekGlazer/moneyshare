import { Group } from "../Models/General";
import { firebase } from "./../firebase/config";

const db = firebase.firestore()

export const addGruop = (group: Group) => {
    return db.collection("Groups").add(group)
}

export const getAllGroups = (setDbGroups: React.Dispatch<React.SetStateAction<Group[]>>, groups: Group[]) => {
    db.collection("Groups").onSnapshot(
        querySnapshot => {
            const newEntities: Group[] = []
            querySnapshot.forEach(doc => {
                const entity: Group = (doc.data() as Group)
                // entity.id = doc.id
                newEntities.push(entity)
            });
            setDbGroups([...newEntities, ...groups])
        },
        error => {
            console.log(error)
        }
    )
}