
import React from "react";
import { Text, View } from "react-native";
import { AntDesign } from "@expo/vector-icons";

interface Props {
    name: string
}

const MemberInGroup: React.FC<Props> = ({ name }) => {

    return (
        <View style={{ flexDirection: "row", justifyContent: "space-between", width: "80%" }}>
            <Text style={{ fontSize: 18, fontWeight: "bold" }}>{name}</Text>
            <AntDesign name="close" onPress={() => console.log("delete member")} size={20} color="blue" />
        </View>
    );

};


export default MemberInGroup;
