
import { MaterialIcons, AntDesign } from "@expo/vector-icons";
import React, { useState, useEffect, createContext, useContext } from "react";
import { StyleSheet, Text, View, TextInput, Alert, TouchableOpacity } from "react-native";
import { Button, Divider } from "react-native-paper";
import { getAllGroups } from "../../Api/Queries";
import { userContext } from "./../../AppContext";
import { Group, User } from "../../Models/General";
import { initialcategories } from "../Service";
import { firebase } from "./../../firebase/config";
import { MainNavigationProp } from "./../../Routing/types";
import { MainRoutes } from "./../../Routing/routes";

interface Props {
    navigation: MainNavigationProp<MainRoutes.Home>
}

const db = firebase.firestore()

const AllGroups: React.FC<Props> = ({ navigation }) => {

    const [groups, setGroups] = useState<Group[]>([]);

    const globalUser = useContext(userContext).user as User

    useEffect(() => {
        const currentGroups: any[] = []
        db.collection("Groups").where("userID", "==", globalUser.id)
            .onSnapshot(
                querySnapshot => {
                    const newEntities: any[] = []
                    querySnapshot.forEach(doc => {
                        const entity = doc.data()
                        // entity.id = doc.id
                        newEntities.push(entity)
                    });
                    setGroups(newEntities)
                },
                error => {
                    console.log(error)
                }
            )
    }, [])

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: "Groups",
            headerRight: () => (
                <Button onPress={() => { navigation.navigate(MainRoutes.AddGroup) }} color="#2970E9" >
                    add Group
                </Button>
            ),
        });
    }, [navigation,]);

    const groupClicked = (group: Group) => {
        navigation.navigate(MainRoutes.MenuGroup, { group: group })
    }


    return (

        <View >
            {/* <Text>Total Groups{groups.length}</Text> */}
            {groups.map(group => {
                return (
                    <TouchableOpacity key={group.description + group.members[0]} onPress={() => groupClicked(group)}>
                        <View style={{ flexDirection: "row", marginTop: 10, marginLeft: 10, justifyContent: "space-between" }}>
                            <View style={{ flexDirection: "row" }}>

                                <MaterialIcons name={group.category.icon as any} size={40} color="#2970E9" ></MaterialIcons>
                                <View style={{ marginLeft: 10 }}>
                                    <Text style={{ fontWeight: "400", fontSize: 16 }}>{group.title}</Text>
                                    <Text style={{ fontWeight: "100", fontSize: 12 }}>{group.description}</Text>
                                </View>
                            </View>
                            <AntDesign name="right" style={{ marginTop: 15, marginRight: 15 }}></AntDesign>
                        </View>
                        <Divider style={{ marginTop: 10 }} />
                    </TouchableOpacity>
                )
            })}

        </View>

    );
}

const styles = StyleSheet.create({
});

export default AllGroups;
