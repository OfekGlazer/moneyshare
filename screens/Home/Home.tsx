import React, { useState, useEffect, useContext } from "react";
import { StyleSheet, Text, View } from "react-native";
import { firebase } from "./../../firebase/config";
import { userContext } from "./../../AppContext";
import { ActivityIndicator, Button, Colors } from 'react-native-paper';
import { MainNavigationProp, MainRouteProp } from "./../../Routing/types";
import { MainRoutes } from "./../../Routing/routes";

interface Props {
    navigation: MainNavigationProp<MainRoutes.Home>,
    route: MainRouteProp<MainRoutes.Home>
}

const db = firebase.firestore()

enum ScreenState {
    Loading = "Loading",
    UnConnected = "UnConnected"
}

const Home: React.FC<Props> = ({ navigation, route }) => {

    const [screenState, setScreenState] = useState<ScreenState>(ScreenState.Loading);
    const globalUser = useContext(userContext).user


    console.log(globalUser)

    useEffect(() => {
        if (globalUser === undefined) {
            setScreenState(ScreenState.Loading)
        } else if (globalUser === null) {
            setScreenState(ScreenState.UnConnected)
        } else {
            navigation.navigate(MainRoutes.AllGroups)
        }
    }, [globalUser])

    if (screenState == ScreenState.Loading) {
        return (
            // <h1>Loading..</h1>
            <View style={styles.container}>
                {/* <ActivityIndicator animating={true} color={Colors.blue200} size={150}></ActivityIndicator> */}
                <Text>Loading!!!!!!!!!!!!</Text>
            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <Text style={styles.appTitle}>Loging or Register</Text>
                <View style={{ flexDirection: "row" }}>
                    <Button onPress={() => navigation.navigate(MainRoutes.Login)}>Login</Button>
                    <Button onPress={() => navigation.navigate(MainRoutes.Regsiter)}>Regsiter</Button>

                </View>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        height: 1000,
        marginTop: "50%"
        // backgroundColor: "red"
        // backgroundColor: "#E8EAED",
        // marginBottom: "30px",
    },
    appTitle: {
        fontWeight: "bold",
        fontSize: 40,
        paddingBottom: 15,
    },
});

export default Home;
