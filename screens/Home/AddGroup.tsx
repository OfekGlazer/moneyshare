import React, { useEffect, useState, createContext, useContext } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Dialog, Paragraph, Portal, TextInput, Provider, Divider, Chip, Button } from 'react-native-paper';
import { AntDesign, Ionicons, MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import { Category, Expanse, Group, User } from "../../Models/General";
import { addGruop } from "../../Api/Queries";
import { userContext } from "./../../AppContext";
import { initialcategories } from "../Service";
import { MainNavigationProp } from "./../../Routing/types";
import { MainRoutes } from "./../../Routing/routes";


interface Props {
    navigation: MainNavigationProp<MainRoutes.AddGroup>
    route: any,
    setGroups: React.Dispatch<React.SetStateAction<Group[]>>
}



const AddGroup: React.FC<Props> = ({ navigation, route }) => {

    const globalUser = useContext(userContext).user as User

    const [groupMembers, setGroupMembers] = useState<string[]>([])
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [category, setCategory] = useState<Category>({ icon: "", name: "", isSelected: false });

    const [disableSave, setDisableSave] = useState(true);


    const deleteMemberInGroup = (deletedMember: string): void => {
        setGroupMembers(groupMembers.filter(member => member != deletedMember))
    }

    useEffect(() => {
        checkSave()
    });

    const [categories, setCategories] = useState(initialcategories);

    const categoryClicked = (categoryName: string) => {
        categories.forEach(cat => {
            if (cat.name == categoryName) {
                cat.isSelected = true
                setCategory(cat)
            } else {
                cat.isSelected = false
            }
        })
        setCategories([...categories])
    }

    const saveClicked = (): void => {
        // debugger;
        const currentGroup: Group = {
            id: "",
            userID: globalUser.id,
            title: title,
            description: description,
            category: category,
            members: groupMembers,
            expanses: [],
        }
        addGruop(currentGroup).then((docRef) => {
            // route.params.setGroups([...route.params.groups, currentGroup])
            navigation.navigate(MainRoutes.AllGroups)
        })
            .catch((error) => {
                console.error("Error adding document: ", error);
            });
    }

    const checkSave = (): void => {
        setDisableSave(title == "" || description == "" || category.name == "" || groupMembers.length == 0)
    }


    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: "New Group",
            headerRight: () => (
                <Button onPress={saveClicked} color="#2970E9" disabled={disableSave} >Save</Button>
            ),
        });
    }, [navigation, disableSave]);



    return (
        // <SafeAreaView>
        <View style={styles.container}>
            <TextInput
                dense
                style={styles.inputText}
                onChangeText={setTitle}
                value={title}
                placeholder="Title"
            />
            <TextInput
                style={styles.inputText}
                onChangeText={setDescription}
                value={description}
                placeholder="Description"
            />
            <View>
                <Text style={{ fontWeight: "normal", fontSize: 18 }}>Category</Text>
                <View style={{ marginTop: 5, flexDirection: "row", flexWrap: "wrap" }}>
                    {categories.map(category => {
                        return (
                            <Chip key={category.name} style={[{ alignSelf: "flex-start", marginLeft: 5, marginTop: 5 },
                            category.isSelected ? { backgroundColor: "#69B0EF" } : {}]} onPress={() => categoryClicked(category.name)} >
                                <View style={{ flexDirection: "row" }} >
                                    <MaterialIcons name={category.icon as any} size={15} color="#2970E9" ></MaterialIcons>
                                    <Text style={{ marginLeft: 5 }} >{category.name}</Text>
                                </View>
                            </Chip>
                        )
                    })}
                </View>

            </View>
            <Text style={styles.title}>
                Group members ({groupMembers.length})
            </Text>
            <TouchableOpacity
                onPress={() => navigation.navigate(MainRoutes.AddMember, { setGroupMembers: setGroupMembers, groupMembers: groupMembers })}
            >
                <View style={{ flexDirection: "row", marginTop: 10, marginLeft: 10 }}>

                    <AntDesign name="pluscircle" size={24} color="#2970E9" />
                    <Text style={styles.addMemberButton}>Add member</Text>
                </View>
            </TouchableOpacity>

            <View style={{ alignItems: "center", marginTop: 20 }}>
                {groupMembers.map(member => {
                    return (
                        <View key={member} style={{ flexDirection: "row", justifyContent: "space-between", width: "85%" }}>
                            <Text style={{ fontSize: 18, fontWeight: "500" }}>{member}</Text>
                            <AntDesign name="close" onPress={() => deleteMemberInGroup(member)} size={20} color="#69B0EF" />
                        </View>
                    );
                })}
            </View>



        </View>

    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        // alignItems: "center",
        // backgroundColor: "white",
        // marginBottom: "30px",
    },
    dialogContainer: {
        marginTop: "30%"
    },
    chipItem: {
        alignSelf: "flex-start",
        marginLeft: 2,
        marginTop: 5,
    },
    chipContainer: {
        marginTop: 5,
        flexDirection: "row",
        flexWrap: "wrap"
    },
    divider: {
        marginTop: 10
    },
    addMemberContainer: {
        flexDirection: "row",
    },
    inputText: {
        height: 40,
        width: "85%",
        margin: 12,
        backgroundColor: "white"
        // borderBottomWidth: 1,
        // borderColor: "grey",
        // borderRadius: 1,
        // padding: 10,
    },
    inputTextAddMember: {
        height: 40,
        width: "80%",
        backgroundColor: "white"
    },

    title: {
        // color: "#acacac",
        fontWeight: "bold",
        marginLeft: 10,
        marginTop: 10,
    },
    addMemberButton: {
        fontSize: 18,
        marginLeft: 10,
        color: "#2970E9"

    },

    addMemberButtonInDialog: {
        fontSize: 18,
        marginTop: 15,
        marginLeft: 20,
    },


    buttonDisabled: {
        color: "grey"
    },
    buttonEnable: {
        color: "#2970E9"
    }
});

export default AddGroup;


   // const [group, setGroup] = useState<Group>(initGroup)

    // const handleChange = (prop: keyof Group) => (event: any) => {
    //     setGroup({ ...group, [prop]: event.target.value });
    // };

    // const handleNonInputChange = (prop: keyof Group, value: string[] | Category | Expanse[]) => {
    //     setGroup({ ...group, [prop]: value });
    // }

