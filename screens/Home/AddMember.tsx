import React, { useContext, useEffect, useState, } from "react";
import { Alert, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Dialog, Paragraph, Portal, TextInput, Provider, Divider, Chip, Button } from 'react-native-paper';

import { MainNavigationProp, MainRouteProp } from "./../../Routing/types";
import { MainRoutes } from "./../../Routing/routes";

interface Props {
    navigation: MainNavigationProp<MainRoutes.AddMember>
    item: any,
    route: MainRouteProp<MainRoutes.AddMember>

}



const AddMember: React.FC<Props> = ({ navigation, item, route }) => {


    const [members, setMembers] = useState<string[]>(route.params.groupMembers)
    const [currentMember, setCurrentMember] = useState("");
    const setGroupMembers = route.params.setGroupMembers;


    useEffect(() => {
        setGroupMembers(members)
    }, [members]);

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: "Add Member",
            headerRight: () => (
                <TouchableOpacity
                    onPress={() => navigation.navigate(MainRoutes.AddGroup)}>
                    <Text style={{ fontSize: 18, marginRight: 10, color: "#2970E9" }}>Done</Text>
                </TouchableOpacity>
            ),
        });
    }, [navigation]);



    return (
        // <SafeAreaView>
        <View style={styles.container}>

            <View style={styles.addMemberContainer}>

                <TextInput
                    style={styles.inputTextAddMember}
                    onChangeText={setCurrentMember}
                    value={currentMember}
                    placeholder="name.."
                />

                <TouchableOpacity
                    onPress={() => { setMembers([...members, currentMember]) }}
                    disabled={currentMember == ""}

                >
                    <Text style={[styles.addMemberButtonInDialog, currentMember == "" ? styles.buttonDisabled : styles.buttonEnable]}>Add</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.chipContainer}>
                {members.map(member => {
                    return (<Chip style={styles.chipItem} onClose={() => { console.log("close pressed") }} key={member}>{member}</Chip>)
                })}

            </View>

            <Divider style={styles.divider} />
        </View>

    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        // alignItems: "center",
        // backgroundColor: "white",
        // marginBottom: "30px",
    },
    dialogContainer: {
        marginTop: "30%"
    },
    chipItem: {
        alignSelf: "flex-start",
        marginLeft: 2,
        marginTop: 5,
    },
    chipContainer: {
        marginTop: 5,
        flexDirection: "row",
        flexWrap: "wrap"
    },
    divider: {
        marginTop: 10
    },
    addMemberContainer: {
        marginTop: 15,
        marginLeft: 10,
        flexDirection: "row",
    },
    inputText: {
        height: 40,
        width: "85%",
        margin: 12,
        backgroundColor: "white"
    },
    inputTextAddMember: {
        height: 40,
        width: "80%",
        backgroundColor: "white"
    },

    title: {
        // color: "#acacac",
        fontWeight: "bold",
        marginLeft: 10,
        marginTop: 10,
    },
    addMemberButton: {
        fontSize: 18,
        marginRight: 10,

    },

    addMemberButtonInDialog: {
        fontSize: 18,
        marginTop: 15,
        marginLeft: 20,
    },


    buttonDisabled: {
        color: "grey"
    },
    buttonEnable: {
        color: "#2970E9"
    }
});

export default AddMember;
