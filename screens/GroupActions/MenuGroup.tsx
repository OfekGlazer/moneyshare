import { AntDesign, MaterialIcons } from "@expo/vector-icons";
import React, { useState, useEffect } from "react";
import { SafeAreaView, StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity } from "react-native";
import { Button, Chip } from "react-native-paper";
import { Group } from "../../Models/General";
import { MainRoutes } from "../../Routing/routes";
import { Props } from "./../../Routing/types";
import { Avatar } from 'react-native-paper';

const MenuGroup: React.FC<Props<MainRoutes.MenuGroup>> = ({ navigation, route }) => {

    const [group, setGroup] = useState<Group>(route.params.group);

    console.log(group)

    React.useLayoutEffect(() => {
        navigation.setOptions({
            // title: group.title,
            title: group.title,
            headerTitle: () => (
                <View style={{ flexDirection: "row" }}>
                    <MaterialIcons name={group.category.icon} size={30} color="#69B0EF" />
                    <Text style={{ fontSize: 18, fontWeight: "500", marginLeft: 7, marginTop: 5 }}>{group.title}</Text>
                </View>
            )
        });
    }, [navigation]);

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flexDirection: "row", marginTop: 10 }}>
                <Chip style={{ marginLeft: 5 }} textStyle={styles.chipTextStyle} onClose={() => { console.log("close pressed") }} key="Balances">Balances</Chip>
                <Chip style={{ marginLeft: 5 }} textStyle={styles.chipTextStyle} onClose={() => { console.log("close pressed") }} key="Statistics">Statistics</Chip>
                <Chip style={{ marginLeft: 5 }} textStyle={styles.chipTextStyle} onClose={() => { console.log("close pressed") }} key="Info">Info</Chip>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate(MainRoutes.AddExpense, { group: group })}
                style={{ position: "absolute", bottom: 20, right: 20 }}>
                <Avatar.Icon size={50} icon="plus" color="white" style={{ backgroundColor: "#2970E9" }} />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    chipTextStyle: {
        fontSize: 13,
    },

});


export default MenuGroup;
