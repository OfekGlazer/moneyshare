import React, { useState, useEffect } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Button, Divider, TextInput } from "react-native-paper";
import { MainRoutes } from "../../Routing/routes";
import { Props } from "./../../Routing/types";
import DateTimePicker from '@react-native-community/datetimepicker';

const ChoosePayingMember: React.FC<Props<MainRoutes.ChoosePayingMember>> = ({ navigation, route }) => {

    const [members, setMembers] = useState(route.params.members)

    const [searchText, setSearchText] = useState("")

    useEffect(() => {
        const filteredMembers = searchText == "" ? route.params.members : members.filter(member => member.includes(searchText))
        setMembers(filteredMembers)
    }, [searchText])

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: "Paid by",
        });
    }, [navigation]);

    return (
        <View style={{ flex: 1 }}>
            <TextInput
                dense
                style={{ backgroundColor: "white" }}
                onChangeText={setSearchText}
                value={searchText}
                placeholder="Search"
                underlineColor="#2970E9"
            />
            {members.map((member) => {
                return (
                    <TouchableOpacity style={{ marginTop: 30 }} key={member} onPress={() => { console.log(member + " clicked") }}>
                        <Text style={{ fontSize: 18, paddingBottom: 5, marginLeft: 15 }}>{member}</Text>
                        <Divider style={{}} />
                    </TouchableOpacity>
                )
            })}
        </View>

    );
};

interface DateProps {
    value: Date,
    onChange: (event: any) => void
}

// const DateTimePicker = ({ value, onChange }: DateProps) => {
//     return createElement('input', {
//         type: 'date',
//         value: value,
//         placeholder: "Search...",
//         onChange: onChange,
//         style: { width: "85%", height: 40, border: 0, backgroundColor: "white", marginLeft: 12, marginTop: 10 }
//     })
// }

const styles = StyleSheet.create({
    inputText: {
        height: 40,
        width: "85%",
        margin: 12,
        backgroundColor: "white"
        // borderBottomWidth: 1,
        // borderColor: "grey",
        // borderRadius: 1,
        // padding: 10,
    },

});
export default ChoosePayingMember;
