import React, { useState, useEffect, createElement } from "react";
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";
import { Button, TextInput } from "react-native-paper";
import { Expanse, Group } from "../../Models/General";
import { MainRoutes } from "../../Routing/routes";
import { Props } from "./../../Routing/types";
import WebPicker from "./WebPicker";
// import DateTimePickerModal from "react-native-modal-datetime-picker";
import DateTimePicker from '@react-native-community/datetimepicker';

const AddExpense: React.FC<Props<MainRoutes.AddExpense>> = ({ navigation, route }) => {

    const group: Group = route.params.group;

    const [expanse, setExpanse] = useState<Expanse>({ title: "", paidBy: "", partialPayments: [], amount: "", date: new Date() });
    const [payingMember, setPayingMember] = useState("");

    const handleChangeExpanse = (prop: keyof Expanse) => (value: string) => {
        setExpanse({ ...expanse, [prop]: value });
    };

    const handleAmountChange = (value: string) => {
        setExpanse({ ...expanse, amount: value.replace(/[^0-9]/g, ''), })
    }

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: "Expense",
            headerRight: () => (
                <TouchableOpacity
                    onPress={() => navigation.navigate(MainRoutes.MenuGroup, { group: group })}>
                    <Text style={{ fontSize: 18, marginRight: 10, color: "#2970E9" }}>Save</Text>
                </TouchableOpacity>
            )
        });
    }, [navigation]);

    return (
        <View style={{ flex: 1 }}>
            <TextInput
                dense
                style={styles.inputText}
                onChangeText={handleChangeExpanse('title')}
                value={expanse.title}
                placeholder="Title"
            />
            <TextInput
                style={styles.inputText}
                keyboardType="numeric"
                onChangeText={handleAmountChange}
                value={expanse.amount}
                placeholder="Amount"
            />
            <TextInput
                style={styles.inputText}
                onChangeText={handleChangeExpanse('paidBy')}
                value={expanse.paidBy}
                placeholder="Paid By"
                onFocus={() => navigation.navigate(MainRoutes.ChoosePayingMember, { members: group.members, payingMember: expanse.paidBy, setExpanse: setExpanse })}
            />

        </View>

    );
};

interface DateProps {
    value: Date,
    onChange: (event: any) => void
}

// const DateTimePicker = ({ value, onChange }: DateProps) => {
//     return createElement('input', {
//         type: 'date',
//         value: value,
//         placeholder: "Search...",
//         onChange: onChange,
//         style: { width: "85%", height: 40, border: 0, backgroundColor: "white", marginLeft: 12, marginTop: 10 }
//     })
// }

const styles = StyleSheet.create({
    inputText: {
        height: 40,
        width: "85%",
        margin: 12,
        backgroundColor: "white"
        // borderBottomWidth: 1,
        // borderColor: "grey",
        // borderRadius: 1,
        // padding: 10,
    },

});
export default AddExpense;
